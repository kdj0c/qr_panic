QR Panic
========

A small QR code encoder for DRM Panic in the linux kernel

I use this small project to test the code easily

Due to the Panic constraint, it doesn't allocate memory and does all the work
on the stack or on the provided buffers.
For simplification, it only supports Medium error correction, and apply the
first mask (checkboard). It will draw the smallest QRcode that can contain
the string passed as parameter. It also only use the byte encoding format.

--------
Inspired by those projects licensed with MIT licences:
[qrcode-rust](https://github.com/kennytm/qrcode-rust)
[fast_qr](https://github.com/erwanvivien/fast_qr/)
[qr](https://github.com/bjguillot/qr)
